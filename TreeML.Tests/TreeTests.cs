using NUnit.Framework;

namespace TreeML.Tests
{
    public class TreeTests
    {
        [TestCase("foo\nbar\n", 3)]
        [TestCase("access\n\ttime \\2035 - 28 - 07 13:08:24\n\turl \\/ favicon.png\n\tip \\8.8.8.8\n", 5)]
        [TestCase("\\foo\n\\bar\n", 1)]
        [TestCase("foo\n", 2)]
        [TestCase("foo \\data \\data\n\tbar \\data\n\t\tbaz \\data\n", 4)]
        [TestCase("foo\n\n\n", 2)]
        [TestCase("\\foo\n", 1)]
        public void CheckLength(string input, int length)
            => Assert.That(Tree.Create(input, "").Count, Is.EqualTo(length));

        [TestCase("foo \\1\nbar \\2\n", 1, "foo \\1\n")]
        [TestCase("foo \\1\nbar \\2\n", 2, "bar \\2\n")]
        [TestCase("foo \\1\nbar \\2\n", 0, "\nfoo \\1\nbar \\2\n")]
        public void TestAccessByIndex(string tree, int index, string expected)
        {
            Assert.That(Tree.Create(tree, "")[index].ToString(), Is.EqualTo(expected));
        }

        [TestCase("foo \\1\nbar \\2\n", "foo", "foo \\1\n")]
        [TestCase("foo \\1\nbar \\2\n", "bar", "bar \\2\n")]
        [TestCase("foo \\1\nbar \\2\n\tbarchild \\value\n", "bar", "bar \\2\n")]
        public void TestAccessByName(string tree, string name, string expected)
        {
            Assert.That(Tree.Create(tree, "")[name]!.ToString(), Is.EqualTo(expected));
        }
    }
}