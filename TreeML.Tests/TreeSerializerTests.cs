﻿using NUnit.Framework;

using System;
using System.Runtime.Serialization;

namespace TreeML.Tests;

internal class TreeSerializerTests
{
    [Test]
    public void TestConversion()
    {
        var obj = new SerializableClass("Test string", 10, true, 6.9f);
        var tree = TreeSerializer.Serialize(obj, "Test object");
        var deserialized = TreeSerializer.Deserialize<SerializableClass>(tree);

        Assert.That(deserialized is not null);
        Assert.That(deserialized!.IgnoredBoolField is false);
        Assert.That(deserialized!.IntFieldToSerialize == 10);
        Assert.That(deserialized!.StringFieldToSerialize == "Test string");
        Assert.That(deserialized!.IgnoredFloat == 0f);
    }

    [Serializable]
    internal sealed class SerializableClass : ISerializable
    {
        public SerializableClass()
        {

        }

        public SerializableClass(string s, int i, bool b, float f)
        {
            StringFieldToSerialize = s;
            IntFieldToSerialize = i;
            IgnoredBoolField = b;
            _ignoredFloat = f;
        }

        public string StringFieldToSerialize;

        public int IntFieldToSerialize;

        [NonSerialized]
        public bool IgnoredBoolField;

        private float _ignoredFloat;

        public float IgnoredFloat => _ignoredFloat;

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }
    }
}
