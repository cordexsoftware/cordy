﻿using System.ComponentModel;

namespace TreeML;

public sealed class TreeSerializer
{
    public static T? Deserialize<T>(Tree? tree)
        where T : new()
    {
        return (T?)Deserialize(typeof(T), tree) ?? default;
    }

    public static Tree? Serialize(object? obj, string name)
    {
        if (obj is null)
        {
            return null;
        }

        if (obj is string str)
        {
            return new Tree(name, str);
        }

        var type = obj.GetType();

        if (!type.IsSerializable)
        {
            return null;
        }

        if (type.IsValueType)
        {
            return new Tree(name, obj?.ToString() ?? "");
        }

        var subTrees = type.GetFields()
            .Where(field => !field.GetCustomAttributes(typeof(NonSerializedAttribute), false).Any())
            .Select(field => Serialize(field.GetValue(obj), field.Name))
            .Where(tree => tree is not null)
            .ToList() as List<Tree>;

        return new Tree(name, "", subTrees);
    }

    private static object? Deserialize(Type targetType, Tree? tree)
    {
        if (!targetType.IsSerializable)
        {
            return default;
        }

        if (tree is null)
        {
            return default;
        }

        if (targetType == typeof(string))
        {
            return tree.Value;
        }

        if (targetType.IsValueType)
        {
            var converter = TypeDescriptor.GetConverter(targetType);
            return converter.ConvertFromString(tree.Value);
        }

        var fields = targetType.GetFields();

        var ctor = targetType.GetConstructor(Type.EmptyTypes);

        if (ctor is null)
        {
            throw new ArgumentException($"Target type '{targetType.Name}' should have a public parameterless constructor");
        }

        var obj = ctor.Invoke(Array.Empty<object?>());

        foreach (var field in fields.Where(f => f.FieldType.IsSerializable))
        {
            field.SetValue(obj, Deserialize(field.FieldType, tree[field.Name]));
        }

        return obj;
    }
}