﻿using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;

namespace TreeML;

public class Tree : List<Tree>
{
    private const string Separators = $"\n\t\\ ";

    private const char NewLine = '\n';

    private const char Tab = '\t';

    private const char Space = ' ';

    private const char Marker = '\\';

    [DebuggerStepThrough]
    internal Tree(string name = "", string value = "", List<Tree>? childs = null, string baseUri = "", uint row = 0, uint col = 0)
        : base(childs ?? new List<Tree>())
    {
        Name = name;
        Value = value;
        BaseUri = baseUri;
        Row = row;
        Col = col;
    }

    public string Name { get; private set; }

    public string Value { get; private set; }

    public uint Row { get; private set; }

    public uint Col { get; private set; }

    public string BaseUri { get; private set; }

    public new int Count
    {
        get
        {
            var c = 1;
            ForEach((x) => c += x.Count);
            return c;
        }
    }

    public string Uri => $"{BaseUri}#{Row}:{Col}";

    public Tree? this[string path] => this[path.Split(Space)];

    public Tree? this[params string[] path]
    {
        get
        {
            var current = this;
            foreach (var subPath in path)
            {
                if (current?.FirstOrDefault(subTree => subTree.Name == subPath) is { } subTree)
                {
                    current = subTree;
                }
                else
                {
                    return null;
                }
            }

            return current;
        }
    }

    public List<Tree> this[Range range] => Inline().GetRange(range.Start.Value, range.End.Value - range.Start.Value);

    public new Tree this[int index] => Inline().ElementAt(index);

    public static Tree Create(in string input)
    {
        return Create(input, "");
    }

    public static Tree Create(in string input, in string baseUri)
    {
        var row = 1u;
        var col = 1u;

        var root = new Tree("", "", new List<Tree>(), baseUri, row, col);
        var parent = root;
        var stack = new List<Tree> { root };
        var offset = 0;

        while (offset < input.Length)
        {
            var name = GetUntil(input, ref offset, Separators);

            if (name.Length > 0)
            {
                var next = new Tree(name, baseUri, new List<Tree>(), baseUri, row, col);
                parent.Add(next);
                parent = next;

                var spaces = Skip(input, ref offset, Space);
                col += (uint)(name.Length + spaces);

                continue;
            }

            if (offset >= input.Length)
            {
                break;
            }

            if (input[offset] == Marker)
            {
                var val = GetUntil(input, ref offset, NewLine)[1..];

                if (string.IsNullOrEmpty(parent.Value))
                {
                    parent.Value = val;
                }
                else
                {
                    parent.Value += NewLine + val;
                }
            }

            if (offset >= input.Length)
            {
                break;
            }

            if (input[offset] != NewLine)
            {
                throw new Exception($"Unexpected symbol '{input[0]}' at {parent.Uri}");
            }

            offset++;
            row++;
            var indent = Skip(input, ref offset, Tab);

            if (indent > stack.Count)
            {
                throw new Exception($"Too many TABs at {parent.Uri}");
            }

            col = (uint)indent;

            stack.Add(parent);
            parent = stack[indent];
        }

        return root;
    }

    private static int Skip(in string input, ref int offset, in string items)
    {
        var start = offset;
        while (offset < input.Length && items.Contains(input[offset]))
        {
            offset++;
        }

        return offset - start;
    }

    private static int Skip(in string input, ref int offset, in char item)
    {
        var start = offset;
        while (offset < input.Length && item == input[offset])
        {
            offset++;
        }

        return offset - start;
    }

    public override string ToString()
    {
        return Pipe(new StringBuilder(), "").ToString();
    }

    private StringBuilder Pipe(StringBuilder output, string prefix = "")
    {
        if (!string.IsNullOrEmpty(Name))
        {
            output.Append($"{Name}");
        }

        var chunks = Value.Length > 0 ? Value.Split(NewLine) : Array.Empty<string>();

        if (chunks.Length + base.Count == 1)
        {
            if (chunks.Length > 0)
            {
                output.Append($"{Space}{Marker}{chunks[0]}{NewLine}");
            }
            else
            {
                this.First().Pipe(output, prefix);
            }
        }
        else
        {
            output.Append(NewLine);
            if (Name.Length > 0)
            {
                prefix += Tab;
            }

            foreach (var chunk in chunks)
            {
                output.Append($"{prefix}{Marker}{chunk}{NewLine}");
            }

            foreach (var child in this)
            {
                output.Append(prefix);
                child.Pipe(output, prefix);
            }
        }

        return output;
    }

    public List<Tree> Inline()
    {
        var l = new List<Tree> { this };

        foreach (var child in this)
        {
            if (child.Count > 0)
            {
                l.AddRange(child.Inline());
            }
        }
        return l;
    }

    [DebuggerStepThrough]
    private static string GetUntil(string input, ref int offset, string items)
    {
        var start = offset;
        while (offset < input.Length && !items.Contains(input[offset]))
        {
            offset++;
        }

        return input[start..offset];
    }

    [DebuggerStepThrough]
    private static string GetUntil(string input, ref int offset, char item)
    {
        var start = offset;
        while (offset < input.Length && item != input[offset])
        {
            offset++;
        }

        return input[start..offset];
    }
}